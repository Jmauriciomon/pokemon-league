package pokeleague;
import java.util.Random;

/**
 *
 * @author Carlos Páez
 */
public class Core {
    
    // Hp = Health Power, Ap = Attack Power, cl = clase o tipo, As = Attack Speed
    private int hp, ap, cl, as;
    public String namepoke, nametrainer;
    private Random shuffle = new Random();

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getAp() {
        return ap;
    }

    public void setAp(int ap) {
        this.ap = ap;
    }

    public int getCl() {
        return cl;
    }

    public void setCl(int cl) {
        this.cl = cl;
    }

    public int getAs() {
        return as;
    }

    public void setAs(int as) {
        this.as = as;
    }
    
    // método para ataques
    
    public int ataquePokemon(){
        int apvalue = shuffle.nextInt(50);
        return apvalue;
    }
    
    // método para daños recibidos
    
    public void danio(int hp){
        this.hp-=hp;
    }
}